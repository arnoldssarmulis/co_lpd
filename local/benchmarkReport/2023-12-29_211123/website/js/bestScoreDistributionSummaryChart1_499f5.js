
var chart_bestScoreDistributionSummaryChart1_499f5 = new Chart(document.getElementById('chart_bestScoreDistributionSummaryChart1_499f5'), {
    type: 'boxplot',
    data: {
        labels: [
            'Problem_0', 'Problem_1', 'Problem_2', 'Problem_3', 'Problem_4', 'Problem_5', 'Problem_6', 'Problem_7', 'Problem_8'
        ],
        datasets: [
                {
                    label: 'LAHC 200 (favorite)',
                        borderWidth: 4
,
                    data: [
                                {
                                    min: -51,
                                    max: -51,
                                    q1: -51,
                                    q3: -51,
                                    median: -51,
                                    mean: -51,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -4401,
                                    max: -4401,
                                    q1: -4401,
                                    q3: -4401,
                                    median: -4401,
                                    mean: -4401,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -1221,
                                    max: -1221,
                                    q1: -1221,
                                    q3: -1221,
                                    median: -1221,
                                    mean: -1221,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -4509,
                                    max: -4509,
                                    q1: -4509,
                                    q3: -4509,
                                    median: -4509,
                                    mean: -4509,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -586,
                                    max: -586,
                                    q1: -586,
                                    q3: -586,
                                    median: -586,
                                    mean: -586,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -6449,
                                    max: -6449,
                                    q1: -6449,
                                    q3: -6449,
                                    median: -6449,
                                    mean: -6449,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -1564,
                                    max: -1564,
                                    q1: -1564,
                                    q3: -1564,
                                    median: -1564,
                                    mean: -1564,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -59653,
                                    max: -59653,
                                    q1: -59653,
                                    q3: -59653,
                                    median: -59653,
                                    mean: -59653,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -22613,
                                    max: -22613,
                                    q1: -22613,
                                    q3: -22613,
                                    median: -22613,
                                    mean: -22613,
                                    items: [],
                                    outliers: [],
                                }
                            
                    ]
                }
        ]
    },
    options: {
        animation: false,
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
            title: {
                display: true,
                text: 'Best soft score distribution summary (higher is better)'
            }
        },
        scales: {
            x: {
                display: true
            },
            y: {
                title: {
                    display: true,
                    text: 'Best soft score'
                },
                display: true
            }
        },
watermark: {
    image: "website/webjars/timefold/img/timefold-logo-stacked-positive.svg",
    x: 15,
    y: 15,
    width: 48,
    height: 50,
    opacity: 0.1,
    alignX: "right",
    alignY: "bottom",
    alignToChartArea: true,
    position: "front",
}    },
plugins: [{ 
    id: 'customPlugin',
    beforeDraw: (chart, args, options) => {
          const ctx = chart.canvas.getContext('2d');
          ctx.save();
          ctx.globalCompositeOperation = 'destination-over';
          ctx.fillStyle = 'white';
          ctx.fillRect(0, 0, chart.canvas.width, chart.canvas.height);
          ctx.restore();
    }
}]
});

window.addEventListener('beforeprint', () => {
  chart_bestScoreDistributionSummaryChart1_499f5.resize(1280, 720);
});
window.addEventListener('afterprint', () => {
  chart_bestScoreDistributionSummaryChart1_499f5.resize();
});