
var chart_worstScoreCalculationSpeedDifferencePercentageSummaryChart_eb2c1 = new Chart(document.getElementById('chart_worstScoreCalculationSpeedDifferencePercentageSummaryChart_eb2c1'), {
    type: 'bar',
    data: {
        labels: [
            'Problem_0', 'Problem_1', 'Problem_2', 'Problem_3'
        ],
        datasets: [
            {
                  label: 'LAHC (favorite)',
                  grouped: true,
                    borderWidth: 4
,
                  data: [
                    1.1299435028248588, 2.0161290322580645, 2.4122807017543857, 1.8151815181518154
                  ]
                }, 
{
                  label: 'SimulatedAnnealing',
                  grouped: true,
                    borderWidth: 1
                  ,
                  data: [
                    0, 1.2096774193548387, 0, 0
                  ]
                }, 
{
                  label: 'TabuSearch',
                  grouped: true,
                    borderWidth: 1
                  ,
                  data: [
                    0.5649717514124294, 0, 1.9736842105263157, 2.2277227722772275
                  ]
                }
        ]
    },
    options: {
        animation: false,
        responsive: true,
        maintainAspectRatio: false,
        resizeDelay: 100,
        spanGaps: true,
        plugins: {
            title: {
                display: true,
                text: 'Worst score calculation speed difference percentage summary (higher is better)'
            }
        },
        scales: {
            x: {
                display: true
            },
            y: {
                title: {
                    display: true,
                    text: 'Worst score calculation speed difference percentage'
                },
                ticks: {
                        stepSize: 0.01
                        
                },
                type: 'linear',
                display: true
            }
        },
watermark: {
    image: "website/webjars/timefold/img/timefold-logo-stacked-positive.svg",
    x: 15,
    y: 15,
    width: 48,
    height: 50,
    opacity: 0.1,
    alignX: "right",
    alignY: "bottom",
    alignToChartArea: true,
    position: "front",
}    },
plugins: [{ 
    id: 'customPlugin',
    beforeDraw: (chart, args, options) => {
          const ctx = chart.canvas.getContext('2d');
          ctx.save();
          ctx.globalCompositeOperation = 'destination-over';
          ctx.fillStyle = 'white';
          ctx.fillRect(0, 0, chart.canvas.width, chart.canvas.height);
          ctx.restore();
    }
}]
});

window.addEventListener('beforeprint', () => {
  chart_worstScoreCalculationSpeedDifferencePercentageSummaryChart_eb2c1.resize(1280, 720);
});
window.addEventListener('afterprint', () => {
  chart_worstScoreCalculationSpeedDifferencePercentageSummaryChart_eb2c1.resize();
});