
var chart_bestScoreDistributionSummaryChart1_94b9a = new Chart(document.getElementById('chart_bestScoreDistributionSummaryChart1_94b9a'), {
    type: 'boxplot',
    data: {
        labels: [
            'Problem_0', 'Problem_1', 'Problem_2', 'Problem_3'
        ],
        datasets: [
                {
                    label: 'LAHC (favorite)',
                        borderWidth: 4
,
                    data: [
                                {
                                    min: -18078,
                                    max: -18078,
                                    q1: -18078,
                                    q3: -18078,
                                    median: -18078,
                                    mean: -18078,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -169743,
                                    max: -169743,
                                    q1: -169743,
                                    q3: -169743,
                                    median: -169743,
                                    mean: -169743,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -127719,
                                    max: -127719,
                                    q1: -127719,
                                    q3: -127719,
                                    median: -127719,
                                    mean: -127719,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -121992,
                                    max: -121992,
                                    q1: -121992,
                                    q3: -121992,
                                    median: -121992,
                                    mean: -121992,
                                    items: [],
                                    outliers: [],
                                }
                            
                    ]
                }, 
                {
                    label: 'SimulatedAnnealing',
                        borderWidth: 1
                    ,
                    data: [
                                {
                                    min: -18078,
                                    max: -18078,
                                    q1: -18078,
                                    q3: -18078,
                                    median: -18078,
                                    mean: -18078,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -165791,
                                    max: -165791,
                                    q1: -165791,
                                    q3: -165791,
                                    median: -165791,
                                    mean: -165791,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -127719,
                                    max: -127719,
                                    q1: -127719,
                                    q3: -127719,
                                    median: -127719,
                                    mean: -127719,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -121992,
                                    max: -121992,
                                    q1: -121992,
                                    q3: -121992,
                                    median: -121992,
                                    mean: -121992,
                                    items: [],
                                    outliers: [],
                                }
                            
                    ]
                }, 
                {
                    label: 'TabuSearch',
                        borderWidth: 1
                    ,
                    data: [
                                {
                                    min: -18078,
                                    max: -18078,
                                    q1: -18078,
                                    q3: -18078,
                                    median: -18078,
                                    mean: -18078,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -165791,
                                    max: -165791,
                                    q1: -165791,
                                    q3: -165791,
                                    median: -165791,
                                    mean: -165791,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -127719,
                                    max: -127719,
                                    q1: -127719,
                                    q3: -127719,
                                    median: -127719,
                                    mean: -127719,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -124764,
                                    max: -124764,
                                    q1: -124764,
                                    q3: -124764,
                                    median: -124764,
                                    mean: -124764,
                                    items: [],
                                    outliers: [],
                                }
                            
                    ]
                }
        ]
    },
    options: {
        animation: false,
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
            title: {
                display: true,
                text: 'Best soft score distribution summary (higher is better)'
            }
        },
        scales: {
            x: {
                display: true
            },
            y: {
                title: {
                    display: true,
                    text: 'Best soft score'
                },
                display: true
            }
        },
watermark: {
    image: "website/webjars/timefold/img/timefold-logo-stacked-positive.svg",
    x: 15,
    y: 15,
    width: 48,
    height: 50,
    opacity: 0.1,
    alignX: "right",
    alignY: "bottom",
    alignToChartArea: true,
    position: "front",
}    },
plugins: [{ 
    id: 'customPlugin',
    beforeDraw: (chart, args, options) => {
          const ctx = chart.canvas.getContext('2d');
          ctx.save();
          ctx.globalCompositeOperation = 'destination-over';
          ctx.fillStyle = 'white';
          ctx.fillRect(0, 0, chart.canvas.width, chart.canvas.height);
          ctx.restore();
    }
}]
});

window.addEventListener('beforeprint', () => {
  chart_bestScoreDistributionSummaryChart1_94b9a.resize(1280, 720);
});
window.addEventListener('afterprint', () => {
  chart_bestScoreDistributionSummaryChart1_94b9a.resize();
});