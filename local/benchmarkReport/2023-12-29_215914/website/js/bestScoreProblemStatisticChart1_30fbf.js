
var chart_bestScoreProblemStatisticChart1_30fbf = new Chart(document.getElementById('chart_bestScoreProblemStatisticChart1_30fbf'), {
    type: 'line',
    data: {
        labels: [
            9, 10, 20, 21, 22, 23, 33, 38, 44, 52, 56, 58, 62, 65, 66, 80, 88, 96, 97, 378, 5052, 5066, 5378, 10052
        ],
        datasets: [
            {
                  label: 'LAHC (favorite)',
                        stepped: true,
                        pointRadius: 0,
                    borderWidth: 4
,
                  data: [
                    , -1702, , , , , , , , , -1565, -2329, -3129, -3114, -3030, , , , , , , -3030, , 
                  ]
                }, 
{
                  label: 'SimulatedAnnealing',
                        stepped: true,
                        pointRadius: 0,
                    borderWidth: 1
                  ,
                  data: [
                    -1702, , -2771, -2590, -2583, -2578, -2379, -2374, -2367, , , , , , , -2257, -2183, -2099, -2047, -3312, , , -3312, 
                  ]
                }, 
{
                  label: 'TabuSearch',
                        stepped: true,
                        pointRadius: 0,
                    borderWidth: 1
                  ,
                  data: [
                    , -1702, , , , , , , , -1627, , , , , , , , , , , -2128, , , -2128
                  ]
                }
        ]
    },
    options: {
        animation: false,
        responsive: true,
        maintainAspectRatio: false,
        spanGaps: true,
        plugins: {
            title: {
                display: true,
                text: 'Problem_3 best soft score statistic'
            },
            tooltip: {
                callbacks: {
                        title: function(context) {
                            return humanizeTime(context[0].parsed.x);
                        }
                        
                }
            }
        },
        scales: {
            x: {
                title: {
                    display: true,
                    text: 'Time spent'
                },
                ticks: {
                        stepSize: 100
                        ,
                        callback: function(value, index) {
                            return humanizeTime(value);
                        }
                },
                suggestedMin: 0,
                suggestedMax: 10052,
                type: 'linear',
                display: true
            },
            y: {
                title: {
                    display: true,
                    text: 'Best soft score'
                },
                ticks: {
                        stepSize: 100
                        
                },
                type: 'linear',
                display: true
            }
        },
watermark: {
    image: "website/webjars/timefold/img/timefold-logo-stacked-positive.svg",
    x: 15,
    y: 15,
    width: 48,
    height: 50,
    opacity: 0.1,
    alignX: "right",
    alignY: "bottom",
    alignToChartArea: true,
    position: "front",
}    },
plugins: [{ 
    id: 'customPlugin',
    beforeDraw: (chart, args, options) => {
          const ctx = chart.canvas.getContext('2d');
          ctx.save();
          ctx.globalCompositeOperation = 'destination-over';
          ctx.fillStyle = 'white';
          ctx.fillRect(0, 0, chart.canvas.width, chart.canvas.height);
          ctx.restore();
    }
}]
});

window.addEventListener('beforeprint', () => {
  chart_bestScoreProblemStatisticChart1_30fbf.resize(1280, 720);
});
window.addEventListener('afterprint', () => {
  chart_bestScoreProblemStatisticChart1_30fbf.resize();
});
