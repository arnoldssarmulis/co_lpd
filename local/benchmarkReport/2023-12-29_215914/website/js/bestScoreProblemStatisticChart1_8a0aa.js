
var chart_bestScoreProblemStatisticChart1_8a0aa = new Chart(document.getElementById('chart_bestScoreProblemStatisticChart1_8a0aa'), {
    type: 'line',
    data: {
        labels: [
            10, 28, 32, 33, 34, 35, 49, 54, 56, 64, 70, 71, 197, 221, 326, 327, 329, 331, 343, 348, 376, 400, 402, 407, 411, 416, 421, 424, 428, 657, 816, 830, 833, 990, 1077, 2929, 3286, 4664, 5833, 8147, 9664, 13147
        ],
        datasets: [
            {
                  label: 'LAHC (favorite)',
                        stepped: true,
                        pointRadius: 0,
                    borderWidth: 4
,
                  data: [
                    , -9673, -9662, -9200, -8370, -8285, -8274, -8147, , , , , , -8140, , , , , , , , -8052, -8051, -7977, -7663, -7652, -7068, -6692, -6690, , -6662, , -6534, , , , , , -6534, , , 
                  ]
                }, 
{
                  label: 'SimulatedAnnealing',
                        stepped: true,
                        pointRadius: 0,
                    borderWidth: 1
                  ,
                  data: [
                    -9673, , , , , , , , , -9640, -9023, -8287, -7974, , -7851, -7849, -7844, -7748, , -7663, -7661, , , , , , , , , -7444, , -7118, -6894, , -6729, , -6612, , , -6466, , -6466
                  ]
                }, 
{
                  label: 'TabuSearch',
                        stepped: true,
                        pointRadius: 0,
                    borderWidth: 1
                  ,
                  data: [
                    -9673, , , , , , , , -9152, , , , , , , , , , -8815, , , , , , , , , , , , , , , -8510, , -8063, , -7664, , , -7664, 
                  ]
                }
        ]
    },
    options: {
        animation: false,
        responsive: true,
        maintainAspectRatio: false,
        spanGaps: true,
        plugins: {
            title: {
                display: true,
                text: 'Problem_2 best soft score statistic'
            },
            tooltip: {
                callbacks: {
                        title: function(context) {
                            return humanizeTime(context[0].parsed.x);
                        }
                        
                }
            }
        },
        scales: {
            x: {
                title: {
                    display: true,
                    text: 'Time spent'
                },
                ticks: {
                        stepSize: 100
                        ,
                        callback: function(value, index) {
                            return humanizeTime(value);
                        }
                },
                suggestedMin: 0,
                suggestedMax: 13147,
                type: 'linear',
                display: true
            },
            y: {
                title: {
                    display: true,
                    text: 'Best soft score'
                },
                ticks: {
                        stepSize: 100
                        
                },
                type: 'linear',
                display: true
            }
        },
watermark: {
    image: "website/webjars/timefold/img/timefold-logo-stacked-positive.svg",
    x: 15,
    y: 15,
    width: 48,
    height: 50,
    opacity: 0.1,
    alignX: "right",
    alignY: "bottom",
    alignToChartArea: true,
    position: "front",
}    },
plugins: [{ 
    id: 'customPlugin',
    beforeDraw: (chart, args, options) => {
          const ctx = chart.canvas.getContext('2d');
          ctx.save();
          ctx.globalCompositeOperation = 'destination-over';
          ctx.fillStyle = 'white';
          ctx.fillRect(0, 0, chart.canvas.width, chart.canvas.height);
          ctx.restore();
    }
}]
});

window.addEventListener('beforeprint', () => {
  chart_bestScoreProblemStatisticChart1_8a0aa.resize(1280, 720);
});
window.addEventListener('afterprint', () => {
  chart_bestScoreProblemStatisticChart1_8a0aa.resize();
});
