
var chart_bestScoreDistributionSummaryChart1_77789 = new Chart(document.getElementById('chart_bestScoreDistributionSummaryChart1_77789'), {
    type: 'boxplot',
    data: {
        labels: [
            'Problem_0', 'Problem_1', 'Problem_2', 'Problem_3'
        ],
        datasets: [
                {
                    label: 'LAHC (favorite)',
                        borderWidth: 4
,
                    data: [
                                {
                                    min: -4895,
                                    max: -4895,
                                    q1: -4895,
                                    q3: -4895,
                                    median: -4895,
                                    mean: -4895,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -1221,
                                    max: -1221,
                                    q1: -1221,
                                    q3: -1221,
                                    median: -1221,
                                    mean: -1221,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -6534,
                                    max: -6534,
                                    q1: -6534,
                                    q3: -6534,
                                    median: -6534,
                                    mean: -6534,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -3030,
                                    max: -3030,
                                    q1: -3030,
                                    q3: -3030,
                                    median: -3030,
                                    mean: -3030,
                                    items: [],
                                    outliers: [],
                                }
                            
                    ]
                }, 
                {
                    label: 'SimulatedAnnealing',
                        borderWidth: 1
                    ,
                    data: [
                                {
                                    min: -2599,
                                    max: -2599,
                                    q1: -2599,
                                    q3: -2599,
                                    median: -2599,
                                    mean: -2599,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -1221,
                                    max: -1221,
                                    q1: -1221,
                                    q3: -1221,
                                    median: -1221,
                                    mean: -1221,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -6466,
                                    max: -6466,
                                    q1: -6466,
                                    q3: -6466,
                                    median: -6466,
                                    mean: -6466,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -3312,
                                    max: -3312,
                                    q1: -3312,
                                    q3: -3312,
                                    median: -3312,
                                    mean: -3312,
                                    items: [],
                                    outliers: [],
                                }
                            
                    ]
                }, 
                {
                    label: 'TabuSearch',
                        borderWidth: 1
                    ,
                    data: [
                                {
                                    min: -2416,
                                    max: -2416,
                                    q1: -2416,
                                    q3: -2416,
                                    median: -2416,
                                    mean: -2416,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -1221,
                                    max: -1221,
                                    q1: -1221,
                                    q3: -1221,
                                    median: -1221,
                                    mean: -1221,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -7664,
                                    max: -7664,
                                    q1: -7664,
                                    q3: -7664,
                                    median: -7664,
                                    mean: -7664,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -2128,
                                    max: -2128,
                                    q1: -2128,
                                    q3: -2128,
                                    median: -2128,
                                    mean: -2128,
                                    items: [],
                                    outliers: [],
                                }
                            
                    ]
                }
        ]
    },
    options: {
        animation: false,
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
            title: {
                display: true,
                text: 'Best soft score distribution summary (higher is better)'
            }
        },
        scales: {
            x: {
                display: true
            },
            y: {
                title: {
                    display: true,
                    text: 'Best soft score'
                },
                display: true
            }
        },
watermark: {
    image: "website/webjars/timefold/img/timefold-logo-stacked-positive.svg",
    x: 15,
    y: 15,
    width: 48,
    height: 50,
    opacity: 0.1,
    alignX: "right",
    alignY: "bottom",
    alignToChartArea: true,
    position: "front",
}    },
plugins: [{ 
    id: 'customPlugin',
    beforeDraw: (chart, args, options) => {
          const ctx = chart.canvas.getContext('2d');
          ctx.save();
          ctx.globalCompositeOperation = 'destination-over';
          ctx.fillStyle = 'white';
          ctx.fillRect(0, 0, chart.canvas.width, chart.canvas.height);
          ctx.restore();
    }
}]
});

window.addEventListener('beforeprint', () => {
  chart_bestScoreDistributionSummaryChart1_77789.resize(1280, 720);
});
window.addEventListener('afterprint', () => {
  chart_bestScoreDistributionSummaryChart1_77789.resize();
});