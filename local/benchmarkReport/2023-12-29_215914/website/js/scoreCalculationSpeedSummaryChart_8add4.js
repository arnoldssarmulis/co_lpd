
var chart_scoreCalculationSpeedSummaryChart_8add4 = new Chart(document.getElementById('chart_scoreCalculationSpeedSummaryChart_8add4'), {
    type: 'line',
    data: {
        labels: [
            72, 120, 624, 648
        ],
        datasets: [
            {
                  label: 'LAHC (favorite)',
                    borderWidth: 4
,
                  data: [
                    48734, 42133, 13625, 16728
                  ]
                }, 
{
                  label: 'SimulatedAnnealing',
                    borderWidth: 1
                  ,
                  data: [
                    40680, 46475, 12250, 19671
                  ]
                }, 
{
                  label: 'TabuSearch',
                    borderWidth: 1
                  ,
                  data: [
                    54122, 44002, 18735, 18570
                  ]
                }
        ]
    },
    options: {
        animation: false,
        responsive: true,
        maintainAspectRatio: false,
        spanGaps: true,
        plugins: {
            title: {
                display: true,
                text: 'Score calculation speed summary (higher is better)'
            },
            tooltip: {
                callbacks: {
                }
            }
        },
        scales: {
            x: {
                title: {
                    display: true,
                    text: 'Problem scale'
                },
                ticks: {
                        stepSize: 10
                        
                },
                suggestedMin: 0,
                suggestedMax: 648,
                type: 'linear',
                display: true
            },
            y: {
                title: {
                    display: true,
                    text: 'Score calculation speed per second'
                },
                ticks: {
                        stepSize: 1000
                        
                },
                type: 'linear',
                display: true
            }
        },
watermark: {
    image: "website/webjars/timefold/img/timefold-logo-stacked-positive.svg",
    x: 15,
    y: 15,
    width: 48,
    height: 50,
    opacity: 0.1,
    alignX: "right",
    alignY: "bottom",
    alignToChartArea: true,
    position: "front",
}    },
plugins: [{ 
    id: 'customPlugin',
    beforeDraw: (chart, args, options) => {
          const ctx = chart.canvas.getContext('2d');
          ctx.save();
          ctx.globalCompositeOperation = 'destination-over';
          ctx.fillStyle = 'white';
          ctx.fillRect(0, 0, chart.canvas.width, chart.canvas.height);
          ctx.restore();
    }
}]
});

window.addEventListener('beforeprint', () => {
  chart_scoreCalculationSpeedSummaryChart_8add4.resize(1280, 720);
});
window.addEventListener('afterprint', () => {
  chart_scoreCalculationSpeedSummaryChart_8add4.resize();
});
