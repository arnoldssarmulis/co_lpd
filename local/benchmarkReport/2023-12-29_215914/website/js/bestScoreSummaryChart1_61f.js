
var chart_bestScoreSummaryChart1_61f = new Chart(document.getElementById('chart_bestScoreSummaryChart1_61f'), {
    type: 'bar',
    data: {
        labels: [
            'Problem_0', 'Problem_1', 'Problem_2', 'Problem_3'
        ],
        datasets: [
            {
                  label: 'LAHC (favorite)',
                  grouped: true,
                    borderWidth: 4
,
                  data: [
                    -4895, -1221, -6534, -3030
                  ]
                }, 
{
                  label: 'SimulatedAnnealing',
                  grouped: true,
                    borderWidth: 1
                  ,
                  data: [
                    -2599, -1221, -6466, -3312
                  ]
                }, 
{
                  label: 'TabuSearch',
                  grouped: true,
                    borderWidth: 1
                  ,
                  data: [
                    -2416, -1221, -7664, -2128
                  ]
                }
        ]
    },
    options: {
        animation: false,
        responsive: true,
        maintainAspectRatio: false,
        resizeDelay: 100,
        spanGaps: true,
        plugins: {
            title: {
                display: true,
                text: 'Best soft score summary (higher is better)'
            }
        },
        scales: {
            x: {
                display: true
            },
            y: {
                title: {
                    display: true,
                    text: 'Best soft score'
                },
                ticks: {
                        stepSize: 100
                        
                },
                type: 'linear',
                display: true
            }
        },
watermark: {
    image: "website/webjars/timefold/img/timefold-logo-stacked-positive.svg",
    x: 15,
    y: 15,
    width: 48,
    height: 50,
    opacity: 0.1,
    alignX: "right",
    alignY: "bottom",
    alignToChartArea: true,
    position: "front",
}    },
plugins: [{ 
    id: 'customPlugin',
    beforeDraw: (chart, args, options) => {
          const ctx = chart.canvas.getContext('2d');
          ctx.save();
          ctx.globalCompositeOperation = 'destination-over';
          ctx.fillStyle = 'white';
          ctx.fillRect(0, 0, chart.canvas.width, chart.canvas.height);
          ctx.restore();
    }
}]
});

window.addEventListener('beforeprint', () => {
  chart_bestScoreSummaryChart1_61f.resize(1280, 720);
});
window.addEventListener('afterprint', () => {
  chart_bestScoreSummaryChart1_61f.resize();
});