
var chart_timeSpentScalabilitySummaryChart_9c786 = new Chart(document.getElementById('chart_timeSpentScalabilitySummaryChart_9c786'), {
    type: 'line',
    data: {
        labels: [
            72, 90, 120, 180, 624, 648, 19458
        ],
        datasets: [
            {
                  label: 'LAHC (favorite)',
                    borderWidth: 4
,
                  data: [
                    5002, 5002, 5003, 5294, 5793, 5061, 13251
                  ]
                }, 
{
                  label: 'SimulatedAnnealing',
                    borderWidth: 1
                  ,
                  data: [
                    5003, 5002, 5005, 5433, 15918, 5398, 6496
                  ]
                }, 
{
                  label: 'TabuSearch',
                    borderWidth: 1
                  ,
                  data: [
                    5012, 5002, 5019, 10270, 9786, 10049, 16736
                  ]
                }
        ]
    },
    options: {
        animation: false,
        responsive: true,
        maintainAspectRatio: false,
        spanGaps: true,
        plugins: {
            title: {
                display: true,
                text: 'Time spent scalability summary (lower is better)'
            },
            tooltip: {
                callbacks: {
                        label: function(context) {
                            let label = context.dataset.label || '';
                            return label + ": " + humanizeTime(context.parsed.y);
                        }
                }
            }
        },
        scales: {
            x: {
                title: {
                    display: true,
                    text: 'Problem scale (logarithmic)'
                },
                ticks: {
                },
                suggestedMin: 0,
                suggestedMax: 19458,
                type: 'logarithmic',
                display: true
            },
            y: {
                title: {
                    display: true,
                    text: 'Time spent'
                },
                ticks: {
                        stepSize: 100
                        ,
                        callback: function(value, index, ticks) {
                            return humanizeTime(value);
                        }
                },
                type: 'linear',
                display: true
            }
        },
watermark: {
    image: "website/webjars/timefold/img/timefold-logo-stacked-positive.svg",
    x: 15,
    y: 15,
    width: 48,
    height: 50,
    opacity: 0.1,
    alignX: "right",
    alignY: "bottom",
    alignToChartArea: true,
    position: "front",
}    },
plugins: [{ 
    id: 'customPlugin',
    beforeDraw: (chart, args, options) => {
          const ctx = chart.canvas.getContext('2d');
          ctx.save();
          ctx.globalCompositeOperation = 'destination-over';
          ctx.fillStyle = 'white';
          ctx.fillRect(0, 0, chart.canvas.width, chart.canvas.height);
          ctx.restore();
    }
}]
});

window.addEventListener('beforeprint', () => {
  chart_timeSpentScalabilitySummaryChart_9c786.resize(1280, 720);
});
window.addEventListener('afterprint', () => {
  chart_timeSpentScalabilitySummaryChart_9c786.resize();
});
