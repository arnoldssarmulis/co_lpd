
var chart_bestScoreProblemStatisticChart1_69c36 = new Chart(document.getElementById('chart_bestScoreProblemStatisticChart1_69c36'), {
    type: 'line',
    data: {
        labels: [
            9, 10, 12, 13, 14, 20, 24, 56, 58, 59, 161, 162, 195, 279, 280, 282, 283, 299, 318, 338, 374, 375, 381, 384, 390, 396, 399, 402, 614, 769, 772, 775, 793, 1004, 1023, 1294, 2992, 4786, 5793, 6124, 6325, 9786, 10918, 15918
        ],
        datasets: [
            {
                  label: 'LAHC (favorite)',
                        stepped: true,
                        pointRadius: 0,
                    borderWidth: 4
,
                  data: [
                    , -9673, -9200, -8370, -8285, -8274, -8147, , , , , , -8140, , , , , , , , -8052, -8051, -7977, -7663, -7652, -7068, -6692, -6690, , , , -6662, -6534, , , , , , -6534, , , , , 
                  ]
                }, 
{
                  label: 'SimulatedAnnealing',
                        stepped: true,
                        pointRadius: 0,
                    borderWidth: 1
                  ,
                  data: [
                    -9673, , , , , , , -9640, , -8287, -8217, -7974, , -7851, -7849, -7818, -7748, -7663, -7661, , , , , , , , , , -7444, -7118, -6894, , , , -6729, -6612, , , , -6594, -6466, , -6449, -6449
                  ]
                }, 
{
                  label: 'TabuSearch',
                        stepped: true,
                        pointRadius: 0,
                    borderWidth: 1
                  ,
                  data: [
                    -9673, , , , , , , , -9152, , , , , , , , , , , -8815, , , , , , , , , , , , , , -8510, , , -8063, -7664, , , , -7664, , 
                  ]
                }
        ]
    },
    options: {
        animation: false,
        responsive: true,
        maintainAspectRatio: false,
        spanGaps: true,
        plugins: {
            title: {
                display: true,
                text: 'Problem_5 best soft score statistic'
            },
            tooltip: {
                callbacks: {
                        title: function(context) {
                            return humanizeTime(context[0].parsed.x);
                        }
                        
                }
            }
        },
        scales: {
            x: {
                title: {
                    display: true,
                    text: 'Time spent'
                },
                ticks: {
                        stepSize: 100
                        ,
                        callback: function(value, index) {
                            return humanizeTime(value);
                        }
                },
                suggestedMin: 0,
                suggestedMax: 15918,
                type: 'linear',
                display: true
            },
            y: {
                title: {
                    display: true,
                    text: 'Best soft score'
                },
                ticks: {
                        stepSize: 100
                        
                },
                type: 'linear',
                display: true
            }
        },
watermark: {
    image: "website/webjars/timefold/img/timefold-logo-stacked-positive.svg",
    x: 15,
    y: 15,
    width: 48,
    height: 50,
    opacity: 0.1,
    alignX: "right",
    alignY: "bottom",
    alignToChartArea: true,
    position: "front",
}    },
plugins: [{ 
    id: 'customPlugin',
    beforeDraw: (chart, args, options) => {
          const ctx = chart.canvas.getContext('2d');
          ctx.save();
          ctx.globalCompositeOperation = 'destination-over';
          ctx.fillStyle = 'white';
          ctx.fillRect(0, 0, chart.canvas.width, chart.canvas.height);
          ctx.restore();
    }
}]
});

window.addEventListener('beforeprint', () => {
  chart_bestScoreProblemStatisticChart1_69c36.resize(1280, 720);
});
window.addEventListener('afterprint', () => {
  chart_bestScoreProblemStatisticChart1_69c36.resize();
});
