
var chart_scoreCalculationSpeedSummaryChart_63ce5 = new Chart(document.getElementById('chart_scoreCalculationSpeedSummaryChart_63ce5'), {
    type: 'line',
    data: {
        labels: [
            72, 90, 120, 180, 624, 648, 19458
        ],
        datasets: [
            {
                  label: 'LAHC (favorite)',
                    borderWidth: 4
,
                  data: [
                    62371, 39838, 45936, 31742, 13801, 16446, 1949
                  ]
                }, 
{
                  label: 'SimulatedAnnealing',
                    borderWidth: 1
                  ,
                  data: [
                    56175, 31287, 49922, 22636, 12463, 19548, 3228
                  ]
                }, 
{
                  label: 'TabuSearch',
                    borderWidth: 1
                  ,
                  data: [
                    67638, 43838, 45473, 32141, 18030, 18429, 3616
                  ]
                }
        ]
    },
    options: {
        animation: false,
        responsive: true,
        maintainAspectRatio: false,
        spanGaps: true,
        plugins: {
            title: {
                display: true,
                text: 'Score calculation speed summary (higher is better)'
            },
            tooltip: {
                callbacks: {
                }
            }
        },
        scales: {
            x: {
                title: {
                    display: true,
                    text: 'Problem scale (logarithmic)'
                },
                ticks: {
                },
                suggestedMin: 0,
                suggestedMax: 19458,
                type: 'logarithmic',
                display: true
            },
            y: {
                title: {
                    display: true,
                    text: 'Score calculation speed per second'
                },
                ticks: {
                        stepSize: 1000
                        
                },
                type: 'linear',
                display: true
            }
        },
watermark: {
    image: "website/webjars/timefold/img/timefold-logo-stacked-positive.svg",
    x: 15,
    y: 15,
    width: 48,
    height: 50,
    opacity: 0.1,
    alignX: "right",
    alignY: "bottom",
    alignToChartArea: true,
    position: "front",
}    },
plugins: [{ 
    id: 'customPlugin',
    beforeDraw: (chart, args, options) => {
          const ctx = chart.canvas.getContext('2d');
          ctx.save();
          ctx.globalCompositeOperation = 'destination-over';
          ctx.fillStyle = 'white';
          ctx.fillRect(0, 0, chart.canvas.width, chart.canvas.height);
          ctx.restore();
    }
}]
});

window.addEventListener('beforeprint', () => {
  chart_scoreCalculationSpeedSummaryChart_63ce5.resize(1280, 720);
});
window.addEventListener('afterprint', () => {
  chart_scoreCalculationSpeedSummaryChart_63ce5.resize();
});
