
var chart_bestScorePerTimeSpentSummaryChart1_d8b83 = new Chart(document.getElementById('chart_bestScorePerTimeSpentSummaryChart1_d8b83'), {
    type: 'line',
    data: {
        labels: [
            5002, 5003, 5005, 5012, 5019, 5061, 5294, 5398, 5433, 5793, 6496, 9786, 10049, 10270, 13251, 15918, 16736
        ],
        datasets: [
            {
                  label: 'LAHC (favorite)',
                    borderWidth: 4
,
                  data: [
                    -303, -2596, , , , -3030, -51, , , -6534, , , , , -3785, , 
                  ]
                }, 
{
                  label: 'SimulatedAnnealing',
                    borderWidth: 1
                  ,
                  data: [
                    -303, 0, -2596, , , , , -3312, -51, , -5267, , , , , -6449, 
                  ]
                }, 
{
                  label: 'TabuSearch',
                    borderWidth: 1
                  ,
                  data: [
                    -303, , , 0, -2596, , , , , , , -7664, -2128, -575, , , -4696
                  ]
                }
        ]
    },
    options: {
        animation: false,
        responsive: true,
        maintainAspectRatio: false,
        spanGaps: true,
        plugins: {
            title: {
                display: true,
                text: 'Best soft score per time spent summary (higher left is better)'
            },
            tooltip: {
                callbacks: {
                        title: function(context) {
                            return humanizeTime(context[0].parsed.x);
                        }
                        
                }
            }
        },
        scales: {
            x: {
                title: {
                    display: true,
                    text: 'Time spent'
                },
                ticks: {
                        stepSize: 100
                        ,
                        callback: function(value, index) {
                            return humanizeTime(value);
                        }
                },
                suggestedMin: 0,
                suggestedMax: 16736,
                type: 'linear',
                display: true
            },
            y: {
                title: {
                    display: true,
                    text: 'Best soft score'
                },
                ticks: {
                        stepSize: 100
                        
                },
                type: 'linear',
                display: true
            }
        },
watermark: {
    image: "website/webjars/timefold/img/timefold-logo-stacked-positive.svg",
    x: 15,
    y: 15,
    width: 48,
    height: 50,
    opacity: 0.1,
    alignX: "right",
    alignY: "bottom",
    alignToChartArea: true,
    position: "front",
}    },
plugins: [{ 
    id: 'customPlugin',
    beforeDraw: (chart, args, options) => {
          const ctx = chart.canvas.getContext('2d');
          ctx.save();
          ctx.globalCompositeOperation = 'destination-over';
          ctx.fillStyle = 'white';
          ctx.fillRect(0, 0, chart.canvas.width, chart.canvas.height);
          ctx.restore();
    }
}]
});

window.addEventListener('beforeprint', () => {
  chart_bestScorePerTimeSpentSummaryChart1_d8b83.resize(1280, 720);
});
window.addEventListener('afterprint', () => {
  chart_bestScorePerTimeSpentSummaryChart1_d8b83.resize();
});
