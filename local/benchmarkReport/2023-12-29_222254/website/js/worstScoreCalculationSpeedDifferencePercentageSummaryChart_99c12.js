
var chart_worstScoreCalculationSpeedDifferencePercentageSummaryChart_99c12 = new Chart(document.getElementById('chart_worstScoreCalculationSpeedDifferencePercentageSummaryChart_99c12'), {
    type: 'bar',
    data: {
        labels: [
            'Problem_0', 'Problem_1', 'Problem_2', 'Problem_3', 'Problem_4', 'Problem_5', 'Problem_6'
        ],
        datasets: [
            {
                  label: 'LAHC (favorite)',
                  grouped: true,
                    borderWidth: 4
,
                  data: [
                    40.227955469164165, 1.0181866162338091, 11.029817534490432, 0, 27.330840285102436, 10.735777902591671, 0
                  ]
                }, 
{
                  label: 'SimulatedAnnealing',
                  grouped: true,
                    borderWidth: 1
                  ,
                  data: [
                    0, 9.783827765927033, 0, 65.62339661364803, 0, 0, 18.861729295877417
                  ]
                }, 
{
                  label: 'TabuSearch',
                  grouped: true,
                    borderWidth: 1
                  ,
                  data: [
                    41.99063438770101, 0, 20.405874499332445, 85.53104155977425, 40.115703007638956, 44.66821792505817, 12.0576431959139
                  ]
                }
        ]
    },
    options: {
        animation: false,
        responsive: true,
        maintainAspectRatio: false,
        resizeDelay: 100,
        spanGaps: true,
        plugins: {
            title: {
                display: true,
                text: 'Worst score calculation speed difference percentage summary (higher is better)'
            }
        },
        scales: {
            x: {
                display: true
            },
            y: {
                title: {
                    display: true,
                    text: 'Worst score calculation speed difference percentage'
                },
                ticks: {
                        stepSize: 1
                        
                },
                type: 'linear',
                display: true
            }
        },
watermark: {
    image: "website/webjars/timefold/img/timefold-logo-stacked-positive.svg",
    x: 15,
    y: 15,
    width: 48,
    height: 50,
    opacity: 0.1,
    alignX: "right",
    alignY: "bottom",
    alignToChartArea: true,
    position: "front",
}    },
plugins: [{ 
    id: 'customPlugin',
    beforeDraw: (chart, args, options) => {
          const ctx = chart.canvas.getContext('2d');
          ctx.save();
          ctx.globalCompositeOperation = 'destination-over';
          ctx.fillStyle = 'white';
          ctx.fillRect(0, 0, chart.canvas.width, chart.canvas.height);
          ctx.restore();
    }
}]
});

window.addEventListener('beforeprint', () => {
  chart_worstScoreCalculationSpeedDifferencePercentageSummaryChart_99c12.resize(1280, 720);
});
window.addEventListener('afterprint', () => {
  chart_worstScoreCalculationSpeedDifferencePercentageSummaryChart_99c12.resize();
});