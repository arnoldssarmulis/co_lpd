
var chart_worstScoreDifferencePercentageSummaryChart1_25bf4 = new Chart(document.getElementById('chart_worstScoreDifferencePercentageSummaryChart1_25bf4'), {
    type: 'bar',
    data: {
        labels: [
            'Problem_0', 'Problem_1', 'Problem_2', 'Problem_3', 'Problem_4', 'Problem_5', 'Problem_6'
        ],
        datasets: [
            {
                  label: 'LAHC (favorite)',
                  grouped: true,
                    borderWidth: 4
,
                  data: [
                    91.13043478260869, 0, 0, 28.13745965445225, 0, 14.744258872651356, -42.38721804511278
                  ]
                }, 
{
                  label: 'SimulatedAnnealing',
                  grouped: true,
                    borderWidth: 1
                  ,
                  data: [
                    91.13043478260869, 0, 0, 0, 0, 15.853340292275576, -55.639097744360896
                  ]
                }, 
{
                  label: 'TabuSearch',
                  grouped: true,
                    borderWidth: 1
                  ,
                  data: [
                    0, 0, 0, 10.841086007214733, 0, 0, 0
                  ]
                }
        ]
    },
    options: {
        animation: false,
        responsive: true,
        maintainAspectRatio: false,
        resizeDelay: 100,
        spanGaps: true,
        plugins: {
            title: {
                display: true,
                text: 'Worst soft score difference percentage summary (higher is better)'
            }
        },
        scales: {
            x: {
                display: true
            },
            y: {
                title: {
                    display: true,
                    text: 'Worst soft score difference percentage'
                },
                ticks: {
                        stepSize: 1
                        
                },
                type: 'linear',
                display: true
            }
        },
watermark: {
    image: "website/webjars/timefold/img/timefold-logo-stacked-positive.svg",
    x: 15,
    y: 15,
    width: 48,
    height: 50,
    opacity: 0.1,
    alignX: "right",
    alignY: "bottom",
    alignToChartArea: true,
    position: "front",
}    },
plugins: [{ 
    id: 'customPlugin',
    beforeDraw: (chart, args, options) => {
          const ctx = chart.canvas.getContext('2d');
          ctx.save();
          ctx.globalCompositeOperation = 'destination-over';
          ctx.fillStyle = 'white';
          ctx.fillRect(0, 0, chart.canvas.width, chart.canvas.height);
          ctx.restore();
    }
}]
});

window.addEventListener('beforeprint', () => {
  chart_worstScoreDifferencePercentageSummaryChart1_25bf4.resize(1280, 720);
});
window.addEventListener('afterprint', () => {
  chart_worstScoreDifferencePercentageSummaryChart1_25bf4.resize();
});