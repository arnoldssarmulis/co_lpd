
var chart_bestScoreDistributionSummaryChart1_d5feb = new Chart(document.getElementById('chart_bestScoreDistributionSummaryChart1_d5feb'), {
    type: 'boxplot',
    data: {
        labels: [
            'Problem_0', 'Problem_1', 'Problem_2', 'Problem_3', 'Problem_4', 'Problem_5', 'Problem_6'
        ],
        datasets: [
                {
                    label: 'LAHC (favorite)',
                        borderWidth: 4
,
                    data: [
                                {
                                    min: -51,
                                    max: -51,
                                    q1: -51,
                                    q3: -51,
                                    median: -51,
                                    mean: -51,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -2596,
                                    max: -2596,
                                    q1: -2596,
                                    q3: -2596,
                                    median: -2596,
                                    mean: -2596,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: 0,
                                    max: 0,
                                    q1: 0,
                                    q3: 0,
                                    median: 0,
                                    mean: 0,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -3785,
                                    max: -3785,
                                    q1: -3785,
                                    q3: -3785,
                                    median: -3785,
                                    mean: -3785,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -303,
                                    max: -303,
                                    q1: -303,
                                    q3: -303,
                                    median: -303,
                                    mean: -303,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -6534,
                                    max: -6534,
                                    q1: -6534,
                                    q3: -6534,
                                    median: -6534,
                                    mean: -6534,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -3030,
                                    max: -3030,
                                    q1: -3030,
                                    q3: -3030,
                                    median: -3030,
                                    mean: -3030,
                                    items: [],
                                    outliers: [],
                                }
                            
                    ]
                }, 
                {
                    label: 'SimulatedAnnealing',
                        borderWidth: 1
                    ,
                    data: [
                                {
                                    min: -51,
                                    max: -51,
                                    q1: -51,
                                    q3: -51,
                                    median: -51,
                                    mean: -51,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -2596,
                                    max: -2596,
                                    q1: -2596,
                                    q3: -2596,
                                    median: -2596,
                                    mean: -2596,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: 0,
                                    max: 0,
                                    q1: 0,
                                    q3: 0,
                                    median: 0,
                                    mean: 0,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -5267,
                                    max: -5267,
                                    q1: -5267,
                                    q3: -5267,
                                    median: -5267,
                                    mean: -5267,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -303,
                                    max: -303,
                                    q1: -303,
                                    q3: -303,
                                    median: -303,
                                    mean: -303,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -6449,
                                    max: -6449,
                                    q1: -6449,
                                    q3: -6449,
                                    median: -6449,
                                    mean: -6449,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -3312,
                                    max: -3312,
                                    q1: -3312,
                                    q3: -3312,
                                    median: -3312,
                                    mean: -3312,
                                    items: [],
                                    outliers: [],
                                }
                            
                    ]
                }, 
                {
                    label: 'TabuSearch',
                        borderWidth: 1
                    ,
                    data: [
                                {
                                    min: -575,
                                    max: -575,
                                    q1: -575,
                                    q3: -575,
                                    median: -575,
                                    mean: -575,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -2596,
                                    max: -2596,
                                    q1: -2596,
                                    q3: -2596,
                                    median: -2596,
                                    mean: -2596,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: 0,
                                    max: 0,
                                    q1: 0,
                                    q3: 0,
                                    median: 0,
                                    mean: 0,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -4696,
                                    max: -4696,
                                    q1: -4696,
                                    q3: -4696,
                                    median: -4696,
                                    mean: -4696,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -303,
                                    max: -303,
                                    q1: -303,
                                    q3: -303,
                                    median: -303,
                                    mean: -303,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -7664,
                                    max: -7664,
                                    q1: -7664,
                                    q3: -7664,
                                    median: -7664,
                                    mean: -7664,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -2128,
                                    max: -2128,
                                    q1: -2128,
                                    q3: -2128,
                                    median: -2128,
                                    mean: -2128,
                                    items: [],
                                    outliers: [],
                                }
                            
                    ]
                }
        ]
    },
    options: {
        animation: false,
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
            title: {
                display: true,
                text: 'Best soft score distribution summary (higher is better)'
            }
        },
        scales: {
            x: {
                display: true
            },
            y: {
                title: {
                    display: true,
                    text: 'Best soft score'
                },
                display: true
            }
        },
watermark: {
    image: "website/webjars/timefold/img/timefold-logo-stacked-positive.svg",
    x: 15,
    y: 15,
    width: 48,
    height: 50,
    opacity: 0.1,
    alignX: "right",
    alignY: "bottom",
    alignToChartArea: true,
    position: "front",
}    },
plugins: [{ 
    id: 'customPlugin',
    beforeDraw: (chart, args, options) => {
          const ctx = chart.canvas.getContext('2d');
          ctx.save();
          ctx.globalCompositeOperation = 'destination-over';
          ctx.fillStyle = 'white';
          ctx.fillRect(0, 0, chart.canvas.width, chart.canvas.height);
          ctx.restore();
    }
}]
});

window.addEventListener('beforeprint', () => {
  chart_bestScoreDistributionSummaryChart1_d5feb.resize(1280, 720);
});
window.addEventListener('afterprint', () => {
  chart_bestScoreDistributionSummaryChart1_d5feb.resize();
});