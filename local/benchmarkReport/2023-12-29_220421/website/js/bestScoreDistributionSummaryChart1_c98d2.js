
var chart_bestScoreDistributionSummaryChart1_c98d2 = new Chart(document.getElementById('chart_bestScoreDistributionSummaryChart1_c98d2'), {
    type: 'boxplot',
    data: {
        labels: [
            'Problem_0', 'Problem_1', 'Problem_2', 'Problem_3'
        ],
        datasets: [
                {
                    label: 'LAHC',
                        borderWidth: 1
                    ,
                    data: [
                                {
                                    min: -2256,
                                    max: -2256,
                                    q1: -2256,
                                    q3: -2256,
                                    median: -2256,
                                    mean: -2256,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -330086,
                                    max: -330086,
                                    q1: -330086,
                                    q3: -330086,
                                    median: -330086,
                                    mean: -330086,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -14176,
                                    max: -14176,
                                    q1: -14176,
                                    q3: -14176,
                                    median: -14176,
                                    mean: -14176,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -28183,
                                    max: -28183,
                                    q1: -28183,
                                    q3: -28183,
                                    median: -28183,
                                    mean: -28183,
                                    items: [],
                                    outliers: [],
                                }
                            
                    ]
                }, 
                {
                    label: 'SimulatedAnnealing',
                        borderWidth: 1
                    ,
                    data: [
                                {
                                    min: -2256,
                                    max: -2256,
                                    q1: -2256,
                                    q3: -2256,
                                    median: -2256,
                                    mean: -2256,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -330086,
                                    max: -330086,
                                    q1: -330086,
                                    q3: -330086,
                                    median: -330086,
                                    mean: -330086,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -14176,
                                    max: -14176,
                                    q1: -14176,
                                    q3: -14176,
                                    median: -14176,
                                    mean: -14176,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -29704,
                                    max: -29704,
                                    q1: -29704,
                                    q3: -29704,
                                    median: -29704,
                                    mean: -29704,
                                    items: [],
                                    outliers: [],
                                }
                            
                    ]
                }, 
                {
                    label: 'TabuSearch (favorite)',
                        borderWidth: 4
,
                    data: [
                                {
                                    min: -3106,
                                    max: -3106,
                                    q1: -3106,
                                    q3: -3106,
                                    median: -3106,
                                    mean: -3106,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -330086,
                                    max: -330086,
                                    q1: -330086,
                                    q3: -330086,
                                    median: -330086,
                                    mean: -330086,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -14176,
                                    max: -14176,
                                    q1: -14176,
                                    q3: -14176,
                                    median: -14176,
                                    mean: -14176,
                                    items: [],
                                    outliers: [],
                                }
                            , 
                                {
                                    min: -28183,
                                    max: -28183,
                                    q1: -28183,
                                    q3: -28183,
                                    median: -28183,
                                    mean: -28183,
                                    items: [],
                                    outliers: [],
                                }
                            
                    ]
                }
        ]
    },
    options: {
        animation: false,
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
            title: {
                display: true,
                text: 'Best soft score distribution summary (higher is better)'
            }
        },
        scales: {
            x: {
                display: true
            },
            y: {
                title: {
                    display: true,
                    text: 'Best soft score'
                },
                display: true
            }
        },
watermark: {
    image: "website/webjars/timefold/img/timefold-logo-stacked-positive.svg",
    x: 15,
    y: 15,
    width: 48,
    height: 50,
    opacity: 0.1,
    alignX: "right",
    alignY: "bottom",
    alignToChartArea: true,
    position: "front",
}    },
plugins: [{ 
    id: 'customPlugin',
    beforeDraw: (chart, args, options) => {
          const ctx = chart.canvas.getContext('2d');
          ctx.save();
          ctx.globalCompositeOperation = 'destination-over';
          ctx.fillStyle = 'white';
          ctx.fillRect(0, 0, chart.canvas.width, chart.canvas.height);
          ctx.restore();
    }
}]
});

window.addEventListener('beforeprint', () => {
  chart_bestScoreDistributionSummaryChart1_c98d2.resize(1280, 720);
});
window.addEventListener('afterprint', () => {
  chart_bestScoreDistributionSummaryChart1_c98d2.resize();
});