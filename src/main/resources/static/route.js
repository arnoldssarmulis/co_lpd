function getScorePopoverContent(constraint_list)
{
    var popover_content = "";
    constraint_list.forEach((constraint) =>
    {
          if (getHardScore(constraint.score) == 0)
          {
             popover_content = popover_content + constraint.name + " : " + constraint.score + "<br>";
          }
          else
          {
             popover_content = popover_content + "<b>" + constraint.name + " : " + constraint.score + "</b><br>";
          }
    })
    return popover_content;
}

function getEntityPopoverContent(entityId, indictmentMap)
{
    var popover_content = "";
    var visitId = " visit=" + entityId;
    const indictment = indictmentMap[visitId];
    if (indictment != null)
    {
        popover_content = popover_content +
            "Total score: <b>" + indictment.score + "</b> (" + indictment.matchCount + ")<br>";
        indictment.constraintMatches.forEach((match) =>
        {
                  if (getHardScore(match.score) == 0)
                  {
                     popover_content = popover_content + match.constraintName + " : " + match.score + "<br>";
                  } else {
                     popover_content = popover_content + "<b>" + match.constraintName + " : " + match.score + "</b><br>";
                  }
        });
    }
    return popover_content;
}

function getHardScore(score)
{
   return score.slice(0,score.indexOf("hard"))
}

$(function ()
{
    const urlParams = new URLSearchParams(window.location.search);
    const solutionId = urlParams.get('id');

    $.getJSON("/solution/score?id=" + solutionId, function(analysis)
    {
            var badge = "badge bg-danger";
            if (getHardScore(analysis.score)==0) { badge = "badge bg-success"; }
            $("#score_a").attr({"title":"Score Brakedown","data-bs-content":"" + getScorePopoverContent(analysis.constraints) + "","data-bs-html":"true"});
            $("#score_text").text(analysis.score);
            $("#score_text").attr({"class":badge});
    });

    $.getJSON("/solution/solution?id=" + solutionId, function(solution)
    {
        $.getJSON("/solution/indictments?id=" + solutionId, function(indictments)
        {
            renderRoutes(solution, indictments);
            $(function () {
                $('[data-toggle="popover"]').popover()
            })
        });
    });

});

function renderRoutes(solution, indictments)
{
    var indictmentMap = {};
    var storageFinalAmounts = {};
    indictments.forEach((indictment) => {
         indictmentMap[indictment.indictedObjectID] = indictment;
    })

    const vehicle_div = $("#vehicle_container");

    vehicle_div.append('<h4 id="Heading_Company" class="text-center font-monospace">All used vehicles are shown here</h4>')
    var vehicleAccordionId = "vehicleAccordion";
    var vehicleAccordionHtml = '<div class="accordion" id="' + vehicleAccordionId + '">';
    var timesNewRoman = ' style="font-family: \'Times New Roman\', Times, serif;" ';
    var vehicleCount = 1;
    var capacity = 0;

    solution.vehicleList.forEach((vehicle, vehicleIndex) =>
    {
        capacity = vehicle.carryingCapacity;

        const realVisits = vehicle.visits.filter((visit) =>
        {
            return !(visit.storage != null && visit.takenFromStorage === 0);
        });

        var vehicleHeadingId = 'vehicleHeading' + vehicleIndex;
        var vehicleCollapseId = 'vehicleCollapse' + vehicleIndex;

        vehicleAccordionHtml += '<div class="accordion-item">';
        vehicleAccordionHtml += '<h2 class="accordion-header" id="' + vehicleHeadingId + '">';
        vehicleAccordionHtml += '<button class="accordion-button fs-5 fw-bold text-dark" type="button" data-bs-toggle="collapse" data-bs-target="#' + vehicleCollapseId + '" aria-expanded="true" aria-controls="' + vehicleCollapseId
        vehicleAccordionHtml += timesNewRoman + '">';
        vehicleAccordionHtml += 'Vehicle: ' + vehicleCount + ' visitCount:' + realVisits.length
        vehicleAccordionHtml += '</button></h2>';

        vehicleAccordionHtml += '<div id="' + vehicleCollapseId + '" class="accordion-collapse collapse ' + (vehicleIndex === 0 ? 'show' : '') + '" aria-labelledby="' + vehicleHeadingId + '">';
        vehicleAccordionHtml += '<ul class="list-unstyled"' + timesNewRoman + '</ul>';
        vehicleAccordionHtml += '<li class="lead ps-3"> id: ' + vehicle.Id + '</li>';
        vehicleAccordionHtml += '<li class="lead ps-3"> carryingCapacity: ' + vehicle.carryingCapacity + '</li>';
        vehicleAccordionHtml += '<li class="lead ps-3"> costPerDrivenUnit: ' + vehicle.costPerDrivenUnit + '</li>';
        vehicleAccordionHtml += '<li class="lead ps-3"> totalVisitCount: ' + realVisits.length + '</li>';
        vehicleAccordionHtml += '</ul>';
        vehicleAccordionHtml += '<div class="accordion-body">';

        var visitAccordionId = 'visitAccordion' + vehicleIndex;
        vehicleAccordionHtml += '<div class="accordion" id="' + visitAccordionId + '">';

        realVisits.forEach((visit, visitIndex) =>
        {
            if (visit.storage != null)
            {
                if (storageFinalAmounts.hasOwnProperty(visit.storage.Id))
                {
                    storageFinalAmounts[visit.storage.Id] =
                        Math.min(storageFinalAmounts[visit.storage.Id], visit.storageCurrentAmount);
                }
                else
                {
                    storageFinalAmounts[visit.storage.Id] = visit.storageCurrentAmount;
                }
            }

            var visitHeadingId = 'visitHeading' + vehicleIndex + '-' + visitIndex;
            var visitCollapseId = 'visitCollapse' + vehicleIndex + '-' + visitIndex;

            var visitId = " visit=" + visit.Id;
            var visitIndictment = indictmentMap[visitId];
            var isHardScoreIssue = visitIndictment && getHardScore(visitIndictment.score) !== "0";

            vehicleAccordionHtml += '<div class="accordion-item">';
            vehicleAccordionHtml += '<h2 class="accordion-header" id="' + visitHeadingId + '">';
            vehicleAccordionHtml += '<button class="accordion-button collapsed ' + (isHardScoreIssue ? 'text-danger' : '') +
                '" type="button" data-bs-toggle="collapse" data-bs-target="#' + visitCollapseId + '" aria-expanded="false" aria-controls="' + visitCollapseId + '">';

            var cargoAmountColor = '';
            if (visit.cargoAmount <= 0)
            {
                cargoAmountColor = 'text-danger';
            }
            else if (visit.cargoAmount === capacity)
            {
                cargoAmountColor = 'text-warning';
            }
            else
            {
                cargoAmountColor = 'text-secondary';
            }

            vehicleAccordionHtml += 'Visit <strong>' + visitIndex + '</strong>:&nbsp;&nbsp;visitId=<strong>' + visit.Id + '</strong>&nbsp;&nbsp;type=<strong>' +
                (visit.storage != null ? '<span style="color: blue;">StorageRefill</span>' : '<span style="color: green;">ClientDelivery</span>') + '</strong>' +
                (visit.client != null ? '&nbsp;&nbsp;clientId=<strong>' + visit.client.Id + '</strong>&nbsp;&nbsp;clientRequiredAmount=<strong>' + visit.clientRequiredAmount + '</strong>&nbsp;&nbsp;unloadedToClient=<strong>' + visit.unloadedToClient
                    : '&nbsp;&nbsp;storageId=<strong>' + visit.storage.Id + '</strong>&nbsp;&nbsp;takenFromStorage=<strong>' + visit.takenFromStorage + '</strong>&nbsp;&nbsp;storageCurrentAmount=<strong>' + visit.storageCurrentAmount + '</strong>') + '</strong>' +
                '&nbsp;&nbsp;vehicleCargoAmount=<strong> <span class="' + cargoAmountColor + '">' + visit.cargoAmount + '</span></strong>';

            vehicleAccordionHtml += '</button></h2>';

            vehicleAccordionHtml += '<div id="' + visitCollapseId + '" class="accordion-collapse collapse" aria-labelledby="' + visitHeadingId + '">';
            vehicleAccordionHtml += '<div class="accordion-body">';
            vehicleAccordionHtml += getEntityPopoverContent(visit.Id, indictmentMap);
            vehicleAccordionHtml += '</div></div></div>';

        });
        vehicleAccordionHtml += '</div>';
        vehicleAccordionHtml += '</div></div></div>';

        vehicleCount++;
    });

    vehicleAccordionHtml += '</div>';

    vehicleAccordionHtml += "<br/> <h5>Final Storage Amounts:</h5><ul>";
    for (var storageId in storageFinalAmounts)
    {
        vehicleAccordionHtml += "<li>Storage id=" + storageId + ": finalAmount=" + storageFinalAmounts[storageId] + "</li>";
    }
    vehicleAccordionHtml += "</ul>";

    vehicle_div.append(vehicleAccordionHtml);
}



