package as18284.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class UsedStorageAmount
{
    private Integer VehicleId;

    private Integer StorageId;

    private Integer TotalUsedAmount;
}
