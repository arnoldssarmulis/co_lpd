package as18284.solver;

import ai.timefold.solver.core.api.score.buildin.hardsoft.HardSoftScore;
import ai.timefold.solver.core.api.score.stream.Constraint;
import ai.timefold.solver.core.api.score.stream.ConstraintFactory;
import ai.timefold.solver.core.api.score.stream.ConstraintProvider;
import as18284.domain.Visit;


public class StreamCalculator implements ConstraintProvider
{
    @Override
    public Constraint[] defineConstraints(ConstraintFactory constraintFactory)
    {
        return new Constraint[]
            {
                notEnoughAmountInVehicle(constraintFactory),
                notGivenAnythingToClient(constraintFactory),
                clientRequiredAmountPenalty(constraintFactory),

                clientToClientDistanceCost(constraintFactory),
                clientToStorageDistanceCost(constraintFactory),
                vehicleDepoToStorageDistanceCost(constraintFactory),
                storageToClientDistanceCost(constraintFactory),
                storageToStorageCost(constraintFactory)
            };
    }

    //Liks mašīnām braukt uz noliktavu uzpildīties, nodrošina ka mašīna neaizbrauks pie klienta ar nepietiekamu kravu
    public Constraint notEnoughAmountInVehicle(ConstraintFactory constraintFactory)
    {
        return constraintFactory
                .forEach(Visit.class)
                .filter(visit -> visit.IsClientDelivery() && visit.getCargoAmount() < 0)
                .penalize(HardSoftScore.ONE_HARD, visit -> -visit.getCargoAmount())
                .asConstraint("notEnoughAmountInVehicle");
    }

    //Situācija kur atbrauca pie klienta bez kravas
    public Constraint notGivenAnythingToClient(ConstraintFactory constraintFactory)
    {
        return constraintFactory
                .forEach(Visit.class)
                .filter(visit -> visit.IsClientDelivery() && visit.getCargoAmount() == 0
                        && visit.getUnloadedToClient() != null && visit.getUnloadedToClient() == 0 && visit.getClientRequiredAmount() > 0)
                .penalize(HardSoftScore.ONE_SOFT, visit -> visit.getClientRequiredAmount())
                .asConstraint("notGivenAnythingToClient");
    }

    //Jo vairāk klientiem ir nepiegādāta krava jo sliktāk, sarēķina soda apmēru ņemot vērā katra klienta individuālo soda cenu par vienību
    public Constraint clientRequiredAmountPenalty(ConstraintFactory constraintFactory)
    {
        return constraintFactory
                .forEach(Visit.class)
                .filter(visit -> visit.getVehicle() != null && visit.IsClientDelivery() && visit.getUnloadedToClient() != null)
                .filter(visit -> visit.getUnloadedToClient() < visit.getClientRequiredAmount())
                .penalize(HardSoftScore.ONE_SOFT, visit -> (visit.getClientRequiredAmount() - visit.getUnloadedToClient()) *
                        visit.getClient().getCostPerUndeliveredUnit())
                .asConstraint("clientRequiredAmountPenalty");
    }

    //Aprēķina ceļa veiktā attāluma izmaksas kad mašīna brauc no klietna pie klienta
    public Constraint clientToClientDistanceCost(ConstraintFactory constraintFactory)
    {
        return constraintFactory
                .forEach(Visit.class)
                .filter(visit -> visit.IsClientDelivery() && visit.getPrevActual() != null &&
                                 visit.getPrevActual().IsClientDelivery())
                .penalize(HardSoftScore.ONE_SOFT,
                        visit -> (int)visit.getPrevActual().getClient().getLocation()
                                     .distanceCostTo(visit.getClient().getLocation(), visit.getVehicle().getCostPerDrivenUnit()))
                .asConstraint("clientToClientDistanceCost");
    }

    //Aprēķina ceļa veiktā attāluma izmaksas kad mašīna brauc no klietna uz noliktavu
    public Constraint clientToStorageDistanceCost(ConstraintFactory constraintFactory)
    {
        return constraintFactory
                .forEach(Visit.class)
                .filter(visit -> visit.IsStorageRefill() && visit.getPrevActual() != null &&
                        visit.getPrevActual().IsClientDelivery())
                .penalize(HardSoftScore.ONE_SOFT,
                        visit -> (int)visit.getPrevActual().getClient().getLocation()
                                .distanceCostTo(visit.getStorage().getLocation(), visit.getVehicle().getCostPerDrivenUnit()))
                .asConstraint("clientToStorageDistanceCost");
    }

    //Aprēķina ceļa veiktā attāluma izmaksas kad mašīna brauc no depo uz noliktavu
    //Ir jāapskata divas dažādas situācijas kur mašīna uzreiz uzsākot maršrutu iebrauc noliktavā, vai arī iebrauc tajā pēc kādām tukšajām vizītēm
    public Constraint vehicleDepoToStorageDistanceCost(ConstraintFactory constraintFactory)
    {
        return constraintFactory
                .forEach(Visit.class)
                .filter(visit -> visit.getVehicle() != null && visit.IsStorageRefill() && visit.getTakenFromStorage() != null &&
                        ((visit.getPrevActual() == null && visit.getTakenFromStorage() > 0) ||
                                (visit.getPrevActual() != null && visit.getPrevActual().IsStorageRefill() &&
                                visit.getPrevActual().getCargoAmount() == 0 && visit.getCargoAmount() > 0 && visit.getTakenFromStorage() > 0)))
                .penalize(HardSoftScore.ONE_SOFT,
                        visit -> (int)visit.getVehicle().getDepot().distanceCostTo(visit.getStorage().getLocation(), visit.getVehicle().getCostPerDrivenUnit()))
                .asConstraint("vehicleDepoToStorageDistanceCost");
    }

    //Aprēķina ceļa veiktā attāluma izmaksas kad mašīna brauc no noliktavas pie klienta
    public Constraint storageToClientDistanceCost(ConstraintFactory constraintFactory)
    {
        return constraintFactory
                .forEach(Visit.class)
                .filter(visit -> visit.IsClientDelivery() && visit.getPrevActual() != null && visit.getPrevActual().IsStorageRefill())
                .penalize(HardSoftScore.ONE_SOFT,
                        visit -> (int)visit.getPrevActual().getStorage().getLocation()
                                .distanceCostTo(visit.getClient().getLocation(), visit.getVehicle().getCostPerDrivenUnit()))
                .asConstraint("storageToClientDistanceCost");
    }

    //Situācijas kā kur divas noliktavas ir tik tuvu ka ir izdevīgi iebraukt vēl uzpildīties
    public Constraint storageToStorageCost(ConstraintFactory constraintFactory)
    {
        return constraintFactory
                .forEach(Visit.class)
                .filter(visit -> visit.getPrevActual() != null
                        && visit.getTakenFromStorage() != null && visit.getPrevActual().getTakenFromStorage() != null &&
                        visit.IsStorageRefill() && visit.getPrevActual().IsStorageRefill() &&
                        visit.getTakenFromStorage() > 0 && visit.getPrevActual().getTakenFromStorage() > 0)
                .penalize(HardSoftScore.ONE_SOFT,
                        visit -> (int) visit.getPrevActual().getStorage().getLocation()
                                .distanceCostTo(visit.getStorage().getLocation(), visit.getVehicle().getCostPerDrivenUnit()))
                .asConstraint("storageToStorageCost");
    }

}
