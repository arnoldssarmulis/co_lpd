package as18284.solver;

import ai.timefold.solver.core.api.domain.variable.VariableListener;
import ai.timefold.solver.core.api.score.director.ScoreDirector;
import as18284.domain.*;
import as18284.models.UsedStorageAmount;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class CargoAmountListener implements VariableListener<Solution, Visit>
{
    public static List<UsedStorageAmount> usedStorageAmountList = new ArrayList<>();

    @Override
    public void beforeVariableChanged(ScoreDirector<Solution> scoreDirector, Visit visit)
    {

    }

    @Override
    public void afterVariableChanged(ScoreDirector<Solution> scoreDirector, Visit visit)
    {
        //Apstrādā tekošo maršrutu kas ir daļa no mašīnas kopējā maršruta
        Vehicle routeVehicle = GetRouteVehicle(visit);
        if (routeVehicle == null)
        {
            scoreDirector.beforeVariableChanged(visit, "cargoAmount");
            visit.setCargoAmount(0);
            scoreDirector.afterVariableChanged(visit, "cargoAmount");

            return;
        }

        updateStorageAmounts(scoreDirector, routeVehicle, true);
        removeUsedStorageAmountsForVehicle(routeVehicle);

        //Sarēķina cargoAmount izmaiņas atdodot kravu klientiem un uzpildot to noliktavā
        //Apstaigā vizīšu maršrutu tikai vienas mašīnas ietvaros!, get next vienmēr dos nākamo vizīti tai pašai mašīnai!
        Integer cargoAmount = calculatePreRouteStorages(scoreDirector, visit, routeVehicle);

        for (Visit shadowVisit = visit; shadowVisit != null; shadowVisit = shadowVisit.getNext())
        {
            if (shadowVisit.getVehicle() == null)
                continue;

            if (shadowVisit.IsClientDelivery())
            {
                int requiredAmount = shadowVisit.getClientRequiredAmount();

                //Shadow visit.cargoAmount nevar lietot this.cargoAmount jo vecie varianti šeit vēl nav izmainīti!!!!
                //Lai varētu veikt unload operāciju ir jāzin gan klienta aktuālais requiredAmount, gan aktuālais cargoAmount
                int unloadVolume = GetUnloadVolume(requiredAmount, cargoAmount);
                cargoAmount -= unloadVolume;

                updateUnloadVolume(scoreDirector, shadowVisit, cargoAmount, unloadVolume);

                scoreDirector.beforeVariableChanged(shadowVisit, "cargoAmount");
                shadowVisit.setCargoAmount(cargoAmount);
                scoreDirector.afterVariableChanged(shadowVisit, "cargoAmount");
            }
            else if (shadowVisit.IsStorageRefill())
            {
                cargoAmount = doStorageUpdate(scoreDirector, shadowVisit, routeVehicle, cargoAmount);
            }

        }

        updateStorageAmounts(scoreDirector, routeVehicle, false);
        updateActualAllVariables(scoreDirector, visit);
    }

    private void updateActualAllVariables(ScoreDirector<Solution> scoreDirector, Visit visit)
    {
        Visit shadowVisit = visit;
        for (; shadowVisit.getPrev() != null; shadowVisit = shadowVisit.getPrev()) ;

        for (; shadowVisit != null; shadowVisit = shadowVisit.getNext())
            updateActualVariable(scoreDirector, shadowVisit);
    }

    private void updateActualVariable(ScoreDirector<Solution> scoreDirector, Visit visit)
    {
        boolean isVisitSkipable = visit.IsStorageRefill() && (visit.getTakenFromStorage() == null || visit.getTakenFromStorage() == 0);
        if (visit.getPrev() == null || isVisitSkipable)
        {
            scoreDirector.beforeVariableChanged(visit, "prevActual");
            visit.setPrevActual(null);
            scoreDirector.afterVariableChanged(visit, "prevActual");

            return;
        }

        Visit shadowVisit = visit.getPrev();

        for (; shadowVisit != null && shadowVisit.IsStorageRefill() &&
                (shadowVisit.getTakenFromStorage() == null || shadowVisit.getTakenFromStorage() == 0);
             shadowVisit = shadowVisit.getPrev()) ;

        scoreDirector.beforeVariableChanged(visit, "prevActual");
        visit.setPrevActual(shadowVisit);
        scoreDirector.afterVariableChanged(visit, "prevActual");

    }

    //unloadedToClient vērtība jāatjauno tikai ja tam tika atdota krava
    private void updateUnloadVolume(ScoreDirector<Solution> scoreDirector, Visit shadowVisit, int cargoAmount, int unloadVolume)
    {
        if (cargoAmount >= 0)
        {
            scoreDirector.beforeVariableChanged(shadowVisit, "unloadedToClient");
            shadowVisit.setUnloadedToClient(unloadVolume);
            scoreDirector.afterVariableChanged(shadowVisit, "unloadedToClient");
        }
    }

    //Noskaidro aktuālo noliktavas aizpildījumu un aprēķina cik kravu ir iespējams paņemt no tās
    private int takeFromStorage(ScoreDirector<Solution> scoreDirector, Visit visit, Integer cargoAmount)
    {
        if (visit.getVehicle() == null)
            return 0;

        AtomicInteger totalFillVolume = new AtomicInteger();
        Storage storage = visit.getStorage();

        int currentAmount = cargoAmount == null ? visit.getCargoAmount() : cargoAmount;
        if (currentAmount < 0)
            currentAmount = 0;

        usedStorageAmountList.forEach(st ->
        {
            if (Objects.equals(st.getStorageId(), storage.getId()))
            {
                totalFillVolume.addAndGet(st.getTotalUsedAmount());
            }
        });

        int requiredAmount = visit.getRefillVolume() - currentAmount;
        if (requiredAmount == 0)
        {
            int usedAmount = storage.getCargoAmount() - totalFillVolume.get();

            scoreDirector.beforeVariableChanged(visit, "storageCurrentAmount");
            visit.setStorageCurrentAmount(usedAmount);
            scoreDirector.afterVariableChanged(visit, "storageCurrentAmount");

            scoreDirector.beforeVariableChanged(visit, "takenFromStorage");
            visit.setTakenFromStorage(0);
            scoreDirector.afterVariableChanged(visit, "takenFromStorage");

            return 0;
        }

        int taken;
        int avaliableAmount = storage.getCargoAmount() - totalFillVolume.get();

        if (avaliableAmount <= 0)
            taken = 0;
        else
            taken = Math.min(avaliableAmount, requiredAmount);

        scoreDirector.beforeVariableChanged(visit, "takenFromStorage");
        visit.setTakenFromStorage(taken);
        scoreDirector.afterVariableChanged(visit, "takenFromStorage");

        scoreDirector.beforeVariableChanged(visit, "storageCurrentAmount");
        visit.setStorageCurrentAmount(avaliableAmount - taken);
        scoreDirector.afterVariableChanged(visit, "storageCurrentAmount");

        return taken;
    }

    private Integer calculatePreRouteStorages(ScoreDirector<Solution> scoreDirector, Visit visit, Vehicle routeVehicle)
    {
        if (visit.getPrev() == null)
            return 0;

        Visit shadowVisit = visit.getPrev();
        int initialVisitId = shadowVisit.getId();

        for (; shadowVisit.getPrev() != null; shadowVisit = shadowVisit.getPrev()) ;

        int cargoAmount = 0;
        for (; shadowVisit != null; shadowVisit = shadowVisit.getNext())
        {
            if (shadowVisit.getVehicle() == null)
                continue;

            if (shadowVisit.IsClientDelivery())
            {
                int requiredAmount = shadowVisit.getClientRequiredAmount();
                if (cargoAmount < 0 || requiredAmount == 0)
                    continue;

                int unloadVolume = GetUnloadVolume(requiredAmount, cargoAmount);
                cargoAmount -= unloadVolume;

                updateUnloadVolume(scoreDirector, shadowVisit, cargoAmount, unloadVolume);

                scoreDirector.beforeVariableChanged(shadowVisit, "cargoAmount");
                shadowVisit.setCargoAmount(cargoAmount);
                scoreDirector.afterVariableChanged(shadowVisit, "cargoAmount");
            }
            else if (shadowVisit.IsStorageRefill())
            {
                cargoAmount = doStorageUpdate(scoreDirector, shadowVisit, routeVehicle, cargoAmount);
            }

            if (shadowVisit.getId() == initialVisitId)
            {
                return cargoAmount;
            }
        }
        return cargoAmount;
    }

    private int doStorageUpdate(ScoreDirector<Solution> scoreDirector, Visit shadowVisit, Vehicle routeVehicle, int cargoAmount)
    {
        int takenFromStorage = takeFromStorage(scoreDirector, shadowVisit, cargoAmount);
        cargoAmount += takenFromStorage;

        AddToUsedStorageAmount(shadowVisit, routeVehicle, takenFromStorage);

        scoreDirector.beforeVariableChanged(shadowVisit, "cargoAmount");
        shadowVisit.setCargoAmount(cargoAmount);
        scoreDirector.afterVariableChanged(shadowVisit, "cargoAmount");

        return cargoAmount;
    }

    private Vehicle GetRouteVehicle(Visit visit)
    {
        if (visit.getVehicle() != null)
            return visit.getVehicle();

        return null;
    }

    private void AddToUsedStorageAmount(Visit shadowVisit, Vehicle vehicle, int usedAmount)
    {
        if (vehicle == null || shadowVisit.getStorage() == null || usedAmount == 0)
            return;

        Storage storage = shadowVisit.getStorage();
        UsedStorageAmount storageAmount = getUsedStorageAmount(vehicle, storage);
        if (storageAmount != null)
        {
            storageAmount.setTotalUsedAmount(storageAmount.getTotalUsedAmount() + usedAmount);
        }
        else
        {
            usedStorageAmountList.add(new UsedStorageAmount(vehicle.getId(), storage.getId(), usedAmount));
        }
    }

    private void removeUsedStorageAmountsForVehicle(Vehicle vehicle)
    {
        if (vehicle == null || usedStorageAmountList.isEmpty())
        {
            return;
        }
        Integer vehicleId = vehicle.getId();

        usedStorageAmountList.removeIf(usedAmount -> usedAmount.getVehicleId().equals(vehicleId));
    }

    private void updateStorageAmounts(ScoreDirector<Solution> scoreDirector, Vehicle vehicle, boolean shouldIncrement)
    {
        if (vehicle == null || usedStorageAmountList.isEmpty())
        {
            return;
        }

        List<Visit> storageRefillVisits = scoreDirector
                .getWorkingSolution()
                .getVisitList()
                .stream()
                .filter(Visit::IsStorageRefill)
                .toList();

        for (Visit storageRefillVisit : storageRefillVisits)
        {
            Storage storage = storageRefillVisit.getStorage();
            UsedStorageAmount storageAmount = getUsedStorageAmount(vehicle, storage);

            if (storageAmount == null)
            {
                continue;
            }

            Integer visitsStorageAmount = storageRefillVisit.getFinalStorageAmount();
            if (shouldIncrement)
            {
                visitsStorageAmount += storageAmount.getTotalUsedAmount();
            }
            else
            {
                visitsStorageAmount -= storageAmount.getTotalUsedAmount();
            }

            scoreDirector.beforeVariableChanged(storageRefillVisit, "finalStorageAmount");
            storageRefillVisit.setFinalStorageAmount(visitsStorageAmount);
            scoreDirector.afterVariableChanged(storageRefillVisit, "finalStorageAmount");
        }
    }


    private UsedStorageAmount getUsedStorageAmount(Vehicle vehicle, Storage storage)
    {
        return usedStorageAmountList
                .stream()
                .filter(usedAmount ->
                        usedAmount.getVehicleId().equals(vehicle.getId()) &&
                                usedAmount.getStorageId().equals(storage.getId()))
                .findFirst()
                .orElse(null);
    }

    private int GetUnloadVolume(Integer requiredAmount, Integer cargoAmount)
    {
        if (cargoAmount <= 0)
            return requiredAmount;

        if (requiredAmount > cargoAmount)
            return cargoAmount;
        else
            return requiredAmount;
    }

    @Override
    public void beforeEntityAdded(ScoreDirector<Solution> scoreDirector, Visit visit)
    {

    }

    @Override
    public void afterEntityAdded(ScoreDirector<Solution> scoreDirector, Visit visit)
    {

    }

    @Override
    public void beforeEntityRemoved(ScoreDirector<Solution> scoreDirector, Visit visit)
    {

    }

    @Override
    public void afterEntityRemoved(ScoreDirector<Solution> scoreDirector, Visit visit)
    {

    }
}
