package as18284;

import ai.timefold.solver.benchmark.api.PlannerBenchmark;
import ai.timefold.solver.benchmark.api.PlannerBenchmarkFactory;
import as18284.domain.Solution;
import as18284.domain.SolutionJSONIO;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class BenchmarkTests
{
    public static void main(String[] args)
    {
        PlannerBenchmarkFactory benchmarkFactoryFromXML = PlannerBenchmarkFactory
                .createFromXmlResource("BenchmarkConfig.xml");

        String basePath = System.getProperty("user.dir");
        String jsonTestPath = Paths.get(basePath, "jsonTests").toString();

        List<Solution> testCases = new ArrayList<>();
        File[] jsonFiles = new File(jsonTestPath).listFiles();

        if (jsonFiles != null)
        {
            for (File jsonFile : jsonFiles)
            {
                Solution solution = SolutionJSONIO.GetSolutionInput(jsonFile.getPath());

                if (solution != null)
                {
                    testCases.add(solution);
                }
            }
        };

        PlannerBenchmark benchmark = benchmarkFactoryFromXML
                .buildPlannerBenchmark(testCases.toArray());
        benchmark.benchmarkAndShowReportInBrowser();
    }
}
