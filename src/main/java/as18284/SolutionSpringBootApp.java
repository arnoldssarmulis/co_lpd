package as18284;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SolutionSpringBootApp
{
    public static void main(String[] args)
    {
        SpringApplication.run(SolutionSpringBootApp.class, args);
    }
}
