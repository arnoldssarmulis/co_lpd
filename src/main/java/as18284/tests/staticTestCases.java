package as18284.tests;

import as18284.domain.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class staticTestCases
{
    private static Integer visitId = 0;

    private static Solution solution;

    private static final Random _random = new Random();
    private static final String[] NAMES = {"Alice", "Bob", "Charlie", "David", "Eva", "Frank", "Grace", "Henry", "Ivy", "Jack", "Katherine", "Leo", "Mia", "Nathan", "Olivia", "Patrick", "Quinn", "Rachel", "Samuel", "Taylor"};

    public static Solution largeTestCaseGenerator(int utilitiesSize, int visitScale)
    {
        visitId = 0;
        solution = new Solution();

        List<Location> locations = new ArrayList<>();
        for (int i = 0; i < utilitiesSize; i++)
        {
            locations.add(createRandomLocation(i + 1));
        }
        solution.setLocationList(locations);

        List<Vehicle> vehicles = new ArrayList<>();
        for (int i = 0; i < utilitiesSize; i++)
        {
            vehicles.add(createRandomVehicle(i, getRandomLocation()));
        }
        solution.setVehicleList(vehicles);

        List<Client> clients = new ArrayList<>();
        for (int i = 0; i < utilitiesSize; i++)
        {
            clients.add(createRandomClient(i));
        }
        solution.setClientList(clients);

        List<Storage> storages = new ArrayList<>();
        for (int i = 0; i < utilitiesSize / 2; i++)
        {
            storages.add(createRandomStorage(i));
        }
        solution.setStorageList(storages);

        //Klients paziņo cik vizītēs ir gatavs saņemt kravu un cik maksimāli saņemt katrā vizītē (Vēlamais apjoms ko saņemt, nav obligāti piegādāt tik cik prasa)
        for (int i = 0; i < 100 * visitScale; i++)
            createNewClientVisit();

        return solution;
    }

    private static void createNewClientVisit()
    {
        Random random = new Random();
        int requiredAmount = 50 + random.nextInt(950);

        int randomClientIndex = random.nextInt(solution.getClientList().size());
        Client randomClient = solution.getClientList().get(randomClientIndex);

        Visit visit = new Visit(randomClient);
        visit.setId(visitId++);
        visit.setClientRequiredAmount(requiredAmount);
        solution.getVisitList().add(visit);

        solution.getStorageList().forEach(s ->
        {
            Visit storageVisit = new Visit(s);
            storageVisit.setId(visitId++);
            storageVisit.setFinalStorageAmount(storageVisit.getStorage().getCargoAmount());
            solution.getVisitList().add(storageVisit);
        });
    }

    private static Location getRandomLocation()
    {
        Random random = new Random();
        int randomIndex = random.nextInt(solution.getLocationList().size());
        return solution.getLocationList().get(randomIndex);
    }

    private static Location createRandomLocation(int id)
    {
        Random random = new Random();
        int latitude = 1 + random.nextInt(1000);
        int longitude = 1 + random.nextInt(1000);

        return new Location(id, latitude, longitude);
    }

    private static Vehicle createRandomVehicle(int id, Location depotLocation)
    {
        Random random = new Random();

        int carryingCapacity = 100 + random.nextInt(900);
        int averageSpeed = 40 + random.nextInt(60);
        int costPerDrivenUnit = 1 + random.nextInt(10);

        return new Vehicle(id, carryingCapacity, averageSpeed, costPerDrivenUnit, new ArrayList<>(), depotLocation);
    }

    private static Client createRandomClient(int id)
    {
        String name = getRandomName();
        int requiredAmount = generateRandomAmount(500, 5000);
        int costPerUndeliveredUnit = 1 + _random.nextInt(10);
        Location location = getRandomLocation();

        return new Client(id, name, requiredAmount, location, costPerUndeliveredUnit);
    }

    private static String getRandomName()
    {
        int randomIndex = _random.nextInt(NAMES.length);
        return NAMES[randomIndex];
    }

    private static int generateRandomAmount(int min, int max)
    {
        int range = (max - min) / 100 + 1;
        return (_random.nextInt(range) * 100) + min;
    }

    public static Storage createRandomStorage(int id)
    {
        int cargoAmount = 50000 + _random.nextInt(500000);
        Location location = getRandomLocation();

        return new Storage(id, cargoAmount, location);
    }

}
