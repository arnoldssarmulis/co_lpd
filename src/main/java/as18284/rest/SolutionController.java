package as18284.rest;

import ai.timefold.solver.core.api.score.analysis.ScoreAnalysis;
import ai.timefold.solver.core.api.score.buildin.hardsoft.HardSoftScore;
import ai.timefold.solver.core.api.score.constraint.Indictment;
import ai.timefold.solver.core.api.solver.SolutionManager;
import ai.timefold.solver.core.api.solver.SolverManager;
import as18284.domain.Solution;
import as18284.helpers.SimpleIndictmentObject;
import as18284.solver.CargoAmountListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/solution")
public class SolutionController
{
    @Autowired
    private SolverManager<Solution, String> solverManager;
    @Autowired
    private SolutionManager<Solution, HardSoftScore> solutionManager;

    //risinājumu glabāšanai
    private Map<Integer, Solution> solutionMap = new HashMap<>();


    @PostMapping("/solve")
    public void solve(@RequestBody Solution task)
    {
        CargoAmountListener.usedStorageAmountList = new ArrayList<>();

        //Palaidīs risināšanu
        solverManager.solveAndListen(
                task.getId().toString(),
                id -> task,
                //Nosaka ka atrodot jaunu risinājumu tas tiks ievietots risinājumu glabātuvē
                solution -> solutionMap.put(solution.getId(), solution)
        );
    }

    @GetMapping("/solutionList")
    public List<Solution> list()
    {
        return solutionMap.values().stream().toList();
    }

    @GetMapping("/score")
    public ScoreAnalysis<HardSoftScore> score(@RequestParam int id)
    {
        return solutionManager.analyze(solutionMap.get(id));
    }

    @GetMapping("/solution")
    public Solution solution(@RequestParam int id)
    {
        return solutionMap.get(id);
    }

    @GetMapping("/indictments")
    public List<SimpleIndictmentObject> indictments(@RequestParam int id) {
        return solutionManager
                .explain(solutionMap.getOrDefault(id, null))
                .getIndictmentMap().entrySet().stream()
                .map(entry ->
                {
                    Indictment<HardSoftScore> indictment = entry.getValue();
                    return
                        new SimpleIndictmentObject(entry.getKey(),
                                indictment.getScore(),
                                indictment.getConstraintMatchCount(),
                                indictment.getConstraintMatchSet());
                })
                .collect(Collectors.toList());
    }


}
