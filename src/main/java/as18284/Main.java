package as18284;

import ai.timefold.solver.core.api.score.ScoreExplanation;
import ai.timefold.solver.core.api.score.buildin.hardsoft.HardSoftScore;
import ai.timefold.solver.core.api.solver.Solver;
import ai.timefold.solver.core.api.solver.SolutionManager;
import ai.timefold.solver.core.api.solver.SolverFactory;
import as18284.domain.Solution;
import as18284.domain.SolutionJSONIO;
import as18284.tests.staticTestCases;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Paths;

public class Main
{
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args)
    {
        SolverFactory<Solution> solverFactory = SolverFactory
                .createFromXmlResource("SolverConfig.xml");

        String basePath = System.getProperty("user.dir");
        String jsonTestPath = Paths.get(basePath, "jsonTests", "testTwo.json").toString();

        Solution solutionInput = SolutionJSONIO.GetSolutionInput(jsonTestPath);
        solutionInput.printSolutionDetails();

        Solver<Solution> solver = solverFactory.buildSolver();
        Solution solution = solver.solve(solutionInput);

        SolutionManager<Solution, HardSoftScore> manager = SolutionManager.create(solverFactory);
        ScoreExplanation<Solution, HardSoftScore> explanation = manager.explain(solution);

        LOGGER.info(explanation.getSummary());

        solution.print();

    }
}