package as18284.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
@JsonIdentityInfo(scope = Location.class,
        property = "Id",
        generator = ObjectIdGenerators.PropertyGenerator.class)
public class Location
{
    public Integer Id;

    private Integer latitude;
    private Integer longitude;

    public double distanceCostTo(Location otherLocation, int costPerUnit) {
        double deltaX = otherLocation.getLatitude() - this.getLatitude();
        double deltaY = otherLocation.getLongitude() - this.getLongitude();

        return ( Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2)) * costPerUnit);
    }

    @Override
    public String toString() {
        return "Location{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
