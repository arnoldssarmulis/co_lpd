package as18284.domain;


import ai.timefold.solver.core.api.domain.solution.PlanningEntityCollectionProperty;
import ai.timefold.solver.core.api.domain.solution.PlanningScore;
import ai.timefold.solver.core.api.domain.solution.PlanningSolution;
import ai.timefold.solver.core.api.domain.solution.ProblemFactCollectionProperty;
import ai.timefold.solver.core.api.domain.valuerange.ValueRangeProvider;
import ai.timefold.solver.core.api.score.buildin.hardsoft.HardSoftScore;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

@PlanningSolution
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class Solution
{
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);
    private Integer id;

    @PlanningScore
    private HardSoftScore score;

    @ProblemFactCollectionProperty
    @ValueRangeProvider
    @JsonIdentityReference(alwaysAsId = true)
    private List<Visit> visitList = new ArrayList<>();

    @ProblemFactCollectionProperty
    private List<Location> locationList = new ArrayList<>();

    @PlanningEntityCollectionProperty
    private List<Vehicle> vehicleList = new ArrayList<>();

    @ProblemFactCollectionProperty
    private List<Storage> storageList = new ArrayList<>();

    @ProblemFactCollectionProperty
    private List<Client> clientList = new ArrayList<>();

    public void print() {
        this.getVehicleList().forEach(vehicle -> {
            System.out.println("Vehicle id=" + vehicle.getId() + ", carryingCapacity=" + vehicle.getCarryingCapacity());

            vehicle.getVisits()
                    .stream().filter(v -> !v.IsStorageRefill() || v.getTakenFromStorage() > 0)
                    .forEach(visit -> {
                System.out.println("\t" + "type=" + (visit.IsStorageRefill() ? "StorageRefill; " : "ClientDelivery; ") +
                        (visit.IsClientDelivery() ? "cargoAmount:" + visit.getCargoAmount() + ", clientRequiredAmount=" +
                                visit.getClientRequiredAmount() + ", unloadedToClient:" + visit.getUnloadedToClient() :
                                "cargoAmount:" + visit.getCargoAmount() + ", takenFromStorage:" + visit.getTakenFromStorage()) + "\n" +

                        (visit.IsClientDelivery() ? "client:" + visit.getClient().getName() + "(" + visit.getClient().getId() + ")" +
                        " !requiredAmount:" + visit.getClientRequiredAmount() :
                                "storage:" + visit.getStorage().getId() + " currentStorageAmount=" +
                                        visit.getStorageCurrentAmount() + " finalStorageAmount=" + visit.getFinalStorageAmount())
                );
            });
            System.out.println();
        });
    }

    public void printSolutionDetails()
    {
        System.out.println("Visits:");
        for (Visit visit : visitList) {
            System.out.println(visit);
        }

        System.out.println("\nLocations:");
        for (Location location : locationList) {
            System.out.println(location);
        }

        System.out.println("\nStorages:");
        for (Storage storage : storageList) {
            System.out.println(storage);
        }

        System.out.println("\nClients:");
        for (Client client : clientList) {
            System.out.println(client);
        }

        System.out.println("\nVehicles:");
        for (Vehicle vehicle : vehicleList) {
            System.out.println(vehicle);
        }
    }
}
