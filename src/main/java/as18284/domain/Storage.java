package as18284.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
@JsonIdentityInfo(scope = Storage.class,
        property = "Id",
        generator = ObjectIdGenerators.PropertyGenerator.class)
public class Storage
{
    public Integer Id;

    private Integer cargoAmount;

    private Location location;

    @Override
    public String toString() {
        return "Storage{ " +
                " Id=" + Id +
                " }";
    }
}
