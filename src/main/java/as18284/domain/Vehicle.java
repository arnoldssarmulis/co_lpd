package as18284.domain;

import ai.timefold.solver.core.api.domain.entity.PlanningEntity;
import ai.timefold.solver.core.api.domain.variable.PlanningListVariable;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@PlanningEntity @Getter @Setter @AllArgsConstructor @NoArgsConstructor
@JsonIdentityInfo(scope = Vehicle.class,
        property = "Id",
        generator = ObjectIdGenerators.PropertyGenerator.class)
public class Vehicle
{
    public Integer Id;

    private Integer carryingCapacity;
    private Integer averageSpeed;

    private int costPerDrivenUnit;

    @PlanningListVariable
    private List<Visit> visits = new ArrayList<>();

    private Location depot;

    @Override
    public String toString() {
        return "Vehicle{ " +
                " Id=" + Id +
                " }";
    }
}
