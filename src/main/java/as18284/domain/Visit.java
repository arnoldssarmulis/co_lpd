package as18284.domain;

import ai.timefold.solver.core.api.domain.entity.PlanningEntity;
import ai.timefold.solver.core.api.domain.variable.*;
import as18284.solver.CargoAmountListener;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@PlanningEntity @Getter @Setter @AllArgsConstructor @NoArgsConstructor
@JsonIdentityInfo(scope = Visit.class,
        property = "Id",
        generator = ObjectIdGenerators.PropertyGenerator.class)
public class Visit
{
    public Integer Id;

    //uzpilde no storage
    private Integer refillVolume;
    public Integer getRefillVolume()
    {
        if (this.vehicle == null)
            return  0;

        return vehicle.getCarryingCapacity();
    }

    private Client client;

    private Storage storage;

    //Ja konkrētai vizītei izmainās prev vai vehicle tad ir jāpārēķina cargoAmount if (vehicle is chaged || prev is chaged)
    @ShadowVariable(variableListenerClass = CargoAmountListener.class,
            sourceVariableName = "vehicle")
    @ShadowVariable(variableListenerClass = CargoAmountListener.class,
            sourceVariableName = "prev")
    private Integer cargoAmount = 0;

    private Integer clientRequiredAmount;

    @PiggybackShadowVariable(shadowVariableName = "cargoAmount")
    private Integer unloadedToClient;

    @PiggybackShadowVariable(shadowVariableName = "cargoAmount")
    private Integer finalStorageAmount;

    @PiggybackShadowVariable(shadowVariableName = "cargoAmount")
    private Integer takenFromStorage;

    @PiggybackShadowVariable(shadowVariableName = "cargoAmount")
    private Integer storageCurrentAmount;

    //Iepriekšējā up to date vizīte skipojot liekos tukšos apmeklējumus
    @PiggybackShadowVariable(shadowVariableName = "cargoAmount")
    private Visit prevActual;

    @InverseRelationShadowVariable(sourceVariableName = "visits")
    @JsonIdentityReference(alwaysAsId = true)
    private Vehicle vehicle;

    //tai pasai masinai next
    @NextElementShadowVariable(sourceVariableName = "visits")
    @JsonIdentityReference(alwaysAsId = true)
    private Visit next;

    //tai pasai masinai prev
    @PreviousElementShadowVariable(sourceVariableName = "visits")
    @JsonIdentityReference(alwaysAsId = true)
    private Visit prev;

    public Visit(Client client)
    {
        this.client = client;
    }

    public Visit(Storage storage)
    {
        this.storage = storage;
    }

    public boolean IsClientDelivery()
    {
        return this.getClient() != null;
    }

    public boolean IsStorageRefill()
    {
        return this.getStorage() != null;
    }

    @Override
    public String toString()
    {
        return "Visit{ " +
                     "type=" + (this.IsStorageRefill() ? "StorageRefill" : "ClientDelivery") +
                    " id=" + Id +
                    ", cargoAmount=" + cargoAmount +
                    ", clientRequiredAmount=" + clientRequiredAmount +
                    ", unloadedToClient=" + unloadedToClient +
                    ", storageCurrentAmount=" + storageCurrentAmount +
                    ", takenFromStorage=" + takenFromStorage +
                    (this.IsClientDelivery() ? ", client=" + client : ", storage=" + storage) +
                    (this.vehicle != null ? ", vehicleId=" + vehicle.getId() : "") +
                " }";
    }
}
