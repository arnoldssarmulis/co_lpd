package as18284.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
@JsonIdentityInfo(scope = Client.class,
        property = "Id",
        generator = ObjectIdGenerators.PropertyGenerator.class)
public class Client
{
    public Integer Id;

    private String name;

    private Integer requiredAmount;

    private Location location;

    //cena par neizvadātās kravas vienību
    private int costPerUndeliveredUnit;

    @Override
    public String toString() {
        return "Client{ " +
                "Id=" + Id  +
                " }";
    }
}
