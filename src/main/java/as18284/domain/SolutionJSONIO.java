package as18284.domain;

import ai.timefold.solver.jackson.impl.domain.solution.JacksonSolutionFileIO;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class SolutionJSONIO extends JacksonSolutionFileIO<Solution>
{
    private final ObjectMapper objectMapper;
    public SolutionJSONIO()
    {
        super(Solution.class);

        this.objectMapper = new ObjectMapper();
        this.objectMapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
    }

    @Override
    public Solution read(InputStream solutionStream)
    {
        try
        {
            return objectMapper.readValue(solutionStream, Solution.class);
        }
        catch (IOException e)
        {
            throw new IllegalArgumentException("Failed reading inputSolutionStream.", e);
        }
    }


    public static Solution GetSolutionInput(String jsonFilePath)
    {
        SolutionJSONIO jsonIO = new SolutionJSONIO();
        Solution solutionInput = null;

        try
        {
            InputStream jsonFileStream = Files.newInputStream(Paths.get(jsonFilePath));
            solutionInput = jsonIO.read(jsonFileStream);

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return solutionInput;
    }
}
